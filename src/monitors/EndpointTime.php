<?php

namespace PFP\Soap\Monitors;

use PFP\ReportableWS\Monitors\EndpointTimeStorageInterface;

/**
 * Class EndpointTime
 *
 * Monitor the response times of individual service endpoints
 */
class EndpointTime implements \PFP\ReportableWS\CallbackInterface
{

	/**
	 * Storage for the endpoint time information
	 *
	 * @var EndpointTimeStorageInterface
	 */
	private $storage;

	/**
	 * The endpoint namespace
	 *
	 * @var string
	 */
	private $namespace;

	/**
	 * EndpointTime constructor.
	 *
	 * @param EndpointTimeStorageInterface $storage
	 * @param string                       $namespace
	 */
	public function __construct(EndpointTimeStorageInterface $storage, $namespace)
	{
		$this->storage = $storage;
		$this->namespace = $namespace;
	}

	public function addClient(\PFP\ReportableWS\ReportingInterface $client)
	{
		$client->addOnSuccess(function($request, $response, $info) {
			$connectionTime = $info['connect_time'];
			$processingTime = $info['starttransfer_time'] - $info['pretransfer_time'];
			$totalTime = $info['total_time'];
			$header = $request['head'];
			$matches = [];
			foreach($header as $item) {
				$result = preg_match('/^SOAPAction: "((?:http|https):\/\/(?:\w+.)?\w+.\w+(\/\w+)+)"$/', $item, $matches);
				if($result !== false && $result !== 0) {
					break;
				}
			}
			if($matches !== []) {
				$this->storage->save($this->namespace, [
					'url' => $matches[1],
					'name'=> $matches[2],
				], [
					'connect'       => $connectionTime,
					'processing'    => $processingTime,
					'total'         => $totalTime,
				]);
			} else {
				$this->storage->save($this->namespace, [
					'url' => null,
					'name'=> null,
				], [
					'connect'       => $connectionTime,
					'processing'    => $processingTime,
					'total'         => $totalTime,
				]);
			}
		});
	}
}