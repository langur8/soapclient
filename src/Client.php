<?php

namespace dlouhy\Soap;

use DateTime;
use Exception;
use InvalidArgumentException;
use LogicException;
use OutOfBoundsException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionProperty;
use RuntimeException;
use SoapClient;
use SoapFault;
use stdClass;
use Traversable;
use UnexpectedValueException;

/**
 * Soap Client
 *
 * @method onRequest(array $request)
 * @method onResponse(array $request, array $response, array $info)
 * @method onSuccess(array $request, array $response, array $info)
 * @method onFailure(array $request, array $response, array $info, Exception $error)
 */
abstract class Client extends SoapClient
{

    /**
     * @var callable[] function(mixed[] $request):void
     */
    protected array $onRequest = [];

    /**
     * @var callable[] function(mixed[] $request, mixed[]|null $response, mixed[] $info):void
     */
    protected array $onResponse = [];

    /**
     * @var callable[] function(mixed[] $request, mixed[] $response, mixed[] $info):void
     */
    protected array $onSuccess = [];

    /**
     * @var callable[] function(mixed[] $request, mixed[]|null $response, mixed[] $info, \Exception $error):void
     */
    protected array $onFailure = [];

    private ?\CurlMultiHandle $multiHandle;

    /**
     * Holds information about the last request
     *
     * @internal
     *
     * @var array
     */
    protected array $__lastInfo = [
        'request'  => [
            'head' => null,
            'body' => null,
        ],
        'response' => [
            'head' => null,
            'body' => null,
        ],
        'debug'    => null,
    ];

    /**
     * Cookies
     *
     * @var array
     */
    protected array $cookies = [];

    /**
     * cURL Options
     *
     * @var array
     */
    protected array $curlOptions;

    /**
     * Lowercase first character of the root element name
     *
     * @var boolean
     */
    protected bool $lowerCaseFirst;

    /**
     * Keep empty object properties when building the request parameters
     *
     * @var boolean
     */
    protected bool $keepNullProperties;

    /**
     * Original SoapClient Options
     *
     * @var array
     */
    protected array $soapOptions;

    /**
     * Constructor
     *
     * @param string $wsdl
     * @param array  $options
     * @throws SoapFault
     */
    public function __construct($wsdl, array $options = [])
    {
        $context = stream_context_create([
            'ssl' => [
                'verify_peer'       => false,
                'verify_peer_name'  => false,
                'allow_self_signed' => true,
            ],
        ]);

        $options['stream_context'] = $context;

        parent::__construct($wsdl, $options);
        $this->soapOptions = $options;
        $this->curlOptions = [];
        $this->lowerCaseFirst = false;
        $this->keepNullProperties = true;
    }

    /**
     * Event functionality
     *
     * @param $name
     * @param $args
     *
     * @return mixed
     * @throws ReflectionException
     * @internal
     */
    public function __call($name, $args): mixed
    {
        $_this = $this;
        $class = get_class($_this);

        if ($name === '') {
            throw new LogicException("Call to class '$class' method without name.");
        }

        // event functionality
        if (preg_match('#^on[A-Z]#', $name)) {
            $rp = new ReflectionProperty($class, $name);
            if (!$rp->isStatic()) {
                /** @var callable[] $list */
                $list = $_this->$name;
                if (is_array($list) || $list instanceof Traversable) {
                    foreach ($list as $handler) {
                        if (!is_callable($handler)) {
                            $able = is_callable($handler, true, $textual);
                            throw new RuntimeException("Event handler '$textual' is not " . ($able ? 'callable.' : 'valid PHP callback.'));
                        }
                        call_user_func_array($handler, $args);
                    }
                }

                return null;
            }

            throw new LogicException('Event properties has to be public and non-static.');
        }

        return parent::__call($name, $args);
    }

    /**
     * {@inheritDoc}
     *
     * @throws RuntimeException            If connection to the service failed
     * @throws InvalidArgumentException    If invalid request is to be sent
     */
    public function __doRequest(string $request, string $location, string $action, int $version, bool $oneWay = false): ?string
    {
        $ch = curl_init($location);
        if ($ch === false) {
            throw new SoapConnectionException('Soap Connection Error: Curl initialisation failed');
        }

        $headers = [
            'Connection: Close',
            'Content-Type: ' . ($version === 2 ? 'application/soap+xml' : 'text/xml'),
            'SOAPAction: "' . $action . '"',
        ];

        $soapRequest = is_object($request) ?
            $this->getSoapVariables($request, $this->lowerCaseFirst, $this->keepNullProperties) :
            $request;

        $curlOptions = $this->getCurlOptions();

        $curlOptions[CURLOPT_POSTFIELDS] = $soapRequest;
        $curlOptions[CURLOPT_HTTPHEADER] = $headers;
        $curlOptions[CURLINFO_HEADER_OUT] = true;
        $curlOptions[CURLOPT_COOKIE] = $this->parseCookies();

        if (isset($this->soapOptions['login'], $this->soapOptions['password'])) {
            $curlOptions[CURLOPT_USERPWD] = $this->soapOptions['login'] . ':' . $this->soapOptions['password'];
        }

        if (curl_setopt_array($ch, $curlOptions) === false) {
            throw new SoapConnectionException('Failed setting CURL options');
        }
        $this->__lastInfo['request'] = $requestArray = ['head' => $headers, 'body' => $soapRequest];
        $this->onRequest($requestArray);
        try {
            $response = $this->curlExecWithMulti($ch);
            $this->__lastInfo['debug'] = curl_getinfo($ch);
        } catch (Exception $e) {
            curl_close($ch);
            throw new SoapConnectionException('Soap Connection Error: ' . $e->getMessage(), $e->getCode(), $e);
        }

        if (curl_errno($ch)) {
            $err = curl_error($ch);
            $errNo = curl_errno($ch);
            curl_close($ch);
            throw new SoapConnectionException('Soap Connection Error: ' . $err, $errNo);
        }

        curl_close($ch);

        $parsedResponse = $this->parseCurlResponse($response);
        $this->__lastInfo['response'] = ['head' => $parsedResponse['header'], 'body' => $parsedResponse['body']];
        $body = $parsedResponse['body'];

        if ($oneWay === true) {
            return null;
        }

//        if (empty($body)) {
//            $this->onFailure($this->__lastInfo['request']['body'], [$response], $this->__lastInfo['debug'], new SoapEmptyException($this->__lastInfo['debug']));
//        }

        return $body;
    }

    /**
     * {@inheritDoc}
     *
     * @throws RuntimeException    If connection to the service failed
     * @throws SoapFault           Based on the response from the server
     */
    public function __soapCall(string $name, array $args, ?array $options = null, $inputHeaders = null, &$outputHeaders = null): mixed
    {
        try {
            $result = parent::__soapCall($name, $args, $options, $inputHeaders, $outputHeaders);
            $this->onResponse($this->__lastInfo['request'], $this->__lastInfo['response'], $this->__lastInfo['debug']);
            if (isset($this->__soap_fault) && $this->__soap_fault != null) {
                $this->onFailure($this->__lastInfo['request'], $this->__lastInfo['response'], $this->__lastInfo['debug'], $this->__soap_fault);

                return $result;
            }
            $this->onSuccess($this->__lastInfo['request'], $this->__lastInfo['response'], $this->__lastInfo['debug']);

            return $result;
        } catch (RuntimeException $runtimeException) {
            $this->onResponse($this->__lastInfo['request'], $this->__lastInfo['response'], $this->__lastInfo['debug']);
            $this->onFailure($this->__lastInfo['request'], $this->__lastInfo['response'], $this->__lastInfo['debug'], $runtimeException);
            throw $runtimeException;
        } catch (SoapFault $soapFault) {
            $this->onResponse($this->__lastInfo['request'], $this->__lastInfo['response'], $this->__lastInfo['debug']);
            $this->onFailure($this->__lastInfo['request'], $this->__lastInfo['response'], $this->__lastInfo['debug'], $soapFault);
            throw $soapFault;
        }
    }

    /**
     * Maps Result XML Elements to Classes
     *
     * @param object $soapResult
     * @param string $rootClassName
     * @param array  $resultClassMap
     * @param string $resultClassNamespace
     * @param bool   $skipRootObject
     *
     * @return StdClass
     *
     * @throws UnexpectedValueException
     * @throws OutOfBoundsException
     * @throws ReflectionException
     */
    public function mapSoapResult(object $soapResult, string $rootClassName, array $resultClassMap = [], string $resultClassNamespace = '', bool $skipRootObject = false): StdClass
    {
        if (!is_object($soapResult)) {
            throw new UnexpectedValueException('Soap Result is not an object');
        }

        if ($skipRootObject) {
            $objVarsNames = array_keys(get_object_vars($soapResult));
            $rootClassName = reset($objVarsNames);
            $soapResultObj = $this->mapObject($soapResult->$rootClassName, $rootClassName, $resultClassMap, $resultClassNamespace);
        } else {
            $soapResultObj = $this->mapObject($soapResult, $rootClassName, $resultClassMap, $resultClassNamespace);
        }

        return $soapResultObj;
    }

    /**
     * {@inheritDoc}
     */
    public function __setCookie($name, $value = null): void
    {
        $this->cookies[$name] = $value;
    }

    /**
     * Lowercase first character of the root element name
     *
     * Defaults to false
     *
     * @param boolean $lowerCaseFirst
     * @return $this
     */
    public function setLowerCaseFirst(bool $lowerCaseFirst): Client
    {
        $this->lowerCaseFirst = $lowerCaseFirst;

        return $this;
    }

    /**
     * Keep null object properties when building the request parameters
     *
     * Defaults to true
     *
     * @param boolean $keepNullProperties
     * @return $this
     */
    public function setKeepNullProperties(bool $keepNullProperties): Client
    {
        $this->keepNullProperties = $keepNullProperties;

        return $this;
    }

    /**
     * Merge Curl Options
     *
     * @return array
     */
    public function getCurlOptions(): array
    {
        $mandatoryOptions = [
            CURLOPT_POST     => true,
            CURLOPT_HEADER   => true,
            CURLOPT_NOSIGNAL => true, //http://www.php.net/manual/en/function.curl-setopt.php#104597
        ];

        $defaultOptions = [
            CURLOPT_VERBOSE        => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_IPRESOLVE      => CURL_IPRESOLVE_V4,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_2_0,
        ];

        return $mandatoryOptions + $this->curlOptions + $defaultOptions;
    }

    /**
     * Set cURL Options
     *
     * @param array $curlOptions
     * @return $this
     */
    public function setCurlOptions(array $curlOptions): Client
    {
        $this->curlOptions = $curlOptions + $this->curlOptions;

        return $this;
    }

    /**
     * Get SOAP Request Variables
     *
     * Prepares request parameters to be
     * sent in the SOAP Request Body.
     *
     * @param string  $requestObject
     * @param boolean $lowerCaseFirst     Lowercase first character of the root element name
     * @param boolean $keepNullProperties Keep null object properties when building the request parameters
     *
     * @return array
     *
     * @throws InvalidArgumentException
     */
    public function getSoapVariables(string $requestObject, bool $lowerCaseFirst = false, bool $keepNullProperties = true): array
    {
        if (!is_object($requestObject)) {
            throw new InvalidArgumentException('Parameter requestObject is not an object');
        }
        $objectName = $this->getClassNameWithoutNamespaces($requestObject);
        if ($lowerCaseFirst) {
            $objectName = lcfirst($objectName);
        }
        $stdClass = new stdClass();
        $stdClass->$objectName = $requestObject;

        return $this->objectToArray($stdClass, $keepNullProperties);
    }

    public function addOnRequest(callable $function): Client
    {
        $this->onRequest[] = $function;

        return $this;
    }

    public function addOnResponse(callable $function): Client
    {
        $this->onResponse[] = $function;

        return $this;
    }

    public function addOnSuccess(callable $function): Client
    {
        $this->onSuccess[] = $function;

        return $this;
    }

    public function addOnFailure(callable $function): Client
    {
        $this->onFailure[] = $function;

        return $this;
    }

    /**
     * Parse the cookies into a valid HTTP Cookie header value
     *
     * @return string
     */
    protected function parseCookies(): string
    {
        $cookie = '';

        foreach ($this->cookies as $name => $value)
            $cookie .= $name . '=' . $value . '; ';

        return rtrim($cookie, ';');
    }

    /**
     * Get Class Without Namespace Information
     *
     * @param mixed $object
     * @return string
     */
    protected function getClassNameWithoutNamespaces(mixed $object): string
    {
        $class = explode('\\', get_class($object));

        return end($class);
    }

    /**
     * Convert Object to Array
     *
     * This method omits null value properties
     *
     * @param mixed   $obj
     * @param boolean $keepNullProperties Keep null object properties when building the request parameters
     *
     * @return array
     */
    protected function objectToArray(mixed $obj, bool $keepNullProperties = true): array
    {
        $arr = [];
        $arrObj = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($arrObj as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? $this->objectToArray($val, $keepNullProperties) : $val;
            if ($keepNullProperties || $val !== null) {
                $val = ($val === null) ? '' : $val;
                $arr[$key] = is_scalar($val) ? ((string) $val) : $val;
            }
        }

        return $arr;
    }

    /**
     * Map Remote SOAP Objects(stdClass) to local classes
     *
     * @param mixed  $obj            Remote SOAP Object
     * @param string $className      Root (or current) class name
     * @param array  $classMap       Class Mapping
     * @param string $classNamespace Namespace where the local classes are located
     *
     * @return StdClass|array
     *
     * @throws OutOfBoundsException
     * @throws ReflectionException
     * @throws Exception
     */
    protected function mapObject(mixed $obj, string $className, array $classMap = [], string $classNamespace = ''): StdClass|array
    {
        if (is_object($obj)) {
            // Check if there is a mapping.
            if (isset($classMap[$className])) {
                $mappedClassName = $classMap[$className];
            } else {
                if ($classNamespace) {
                    $mappedClassName = str_replace('\\\\', '\\', $classNamespace . '\\' . $className);
                } else {
                    throw new OutOfBoundsException('Missing mapping for element "' . $className . '"');
                }
            }

            // Check if local class exists.
            if (!class_exists($mappedClassName)) {
                throw new OutOfBoundsException('Class not found: "' . $mappedClassName . '"');
            }
            // Get class properties and methods.
            $objProperties = array_keys(get_class_vars($mappedClassName));
            $objMethods = get_class_methods($mappedClassName);

            // Instantiate new mapped object.
            $objInstance = new $mappedClassName();

            // Map remote object to local object.
            $arrObj = get_object_vars($obj);
            foreach ($arrObj as $key => $val) {
                if ($val !== null) {
                    $useSetter = false;
                    if (in_array('set' . $key, $objMethods)) {
                        $useSetter = true;
                    } elseif (!in_array($key, $objProperties)) {
                        throw new OutOfBoundsException('Property "' . $mappedClassName . '::' . $key . '" doesn\'t exist');
                    }

                    // If it's not scalar, recursive call the mapping function
                    if (is_array($val) || is_object($val)) {
                        $val = $this->mapObject($val, $key, $classMap, $classNamespace);
                    }

                    // If there is a setter, use it. If not, set the property directly.
                    if ($useSetter) {
                        $setterName = 'set' . $key;

                        // Check if parameter is \DateTime
                        $reflection = new ReflectionMethod($mappedClassName, $setterName);
                        $params = $reflection->getParameters();
                        if (count($params) !== 1) {
                            throw new OutOfBoundsException('Wrong Argument Count in Setter for property ' . $key);
                        }
                        /* @var $param ReflectionParameter */
                        $param = reset($params);

                        $paramClass = null;
                        $paramType = $param->getType();

                        if ($paramType instanceof ReflectionNamedType && !$paramType->isBuiltin()) {
                            try {
                                $paramClass = new ReflectionClass($paramType->getName());
                            } catch (ReflectionException) {
                                throw new ReflectionException('Invalid type hint for method "' . $setterName . '"');
                            }
                        }
                        if ($paramClass) {
                            $paramClassName = $paramClass->getNamespaceName() . '\\' . $paramClass->getName();
                            // If setter parameter is type-hinted, cast the value before calling the method
                            if ($paramClassName === '\DateTime') {
                                $val = new DateTime($val);
                            }
                        }

                        $objInstance->$setterName($val);
                    } else {
                        $objInstance->$key = $val;
                    }
                }
            }

            return $objInstance;
        } elseif (is_array($obj)) {
            // Value is array.
            $returnArray = [];
            // If array mapping exists, map array elements.
            if (array_key_exists('array|' . $className, $classMap)) {
                $className = 'array|' . $className;
                foreach ($obj as $key => $val) {
                    $returnArray[$key] = $this->mapObject($val, $className, $classMap, $classNamespace);
                }
            } else {
                // If array mapping doesn't exist, return the array.
                $returnArray = $obj;
            }

            return $returnArray;
        } else {
            // Value is scalar. Just return it.
            return $obj;
        }
    }

    /**
     * Parse cURL response into header and body
     *
     * @param string $response
     * @return array
     */
    protected function parseCurlResponse(string $response): array
    {
        // Split the response into header and body by looking for two consecutive CRLFs (\r\n\r\n)
        $headerEndPos = strpos($response, "\r\n\r\n");
        if ($headerEndPos === false) {
            // If no headers are found, assume the response has no header
            return [
                'header' => '',
                'body'   => $response,
            ];
        }
        // Extract the headers (everything before the first \r\n\r\n)
        $header = substr($response, 0, $headerEndPos + 4);
        // The body is everything after the headers
        $body = substr($response, $headerEndPos + 4);

        return [
            'header' => $header,
            'body'   => $body,
        ];
    }

    private function curlExecWithMulti($handle): ?string
    {
        // Create a multi if necessary.
        if (empty($this->multiHandle)) {
            $this->multiHandle = curl_multi_init();
        }

        // Add the handle to be processed.
        curl_multi_add_handle($this->multiHandle, $handle);

        do {
            $status = curl_multi_exec($this->multiHandle, $running);
            if ($status !== CURLM_OK) {
                throw new SoapConnectionException(curl_multi_strerror(curl_multi_errno($this->multiHandle)));
            }

            curl_multi_select($this->multiHandle);
        } while ($running > 0);

        $response = curl_multi_getcontent($handle);
        curl_multi_remove_handle($this->multiHandle, $handle);

        return $response;
    }

}
